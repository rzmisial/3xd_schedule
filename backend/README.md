# Running

To run the backend server, you need to:
* have this directory as your working directory
* set the environmental variable `FLASK_APP` to `app.py`
* execute `flask run`
