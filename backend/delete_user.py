import sys

from database import database
from database.model import User


def ask_user():
    print("Username:")
    return sys.stdin.readline().rstrip("\n")

def delete_user(login):
    user = database.find_user_by_login(login)
    if user:
        database.delete_id(User, user.id)
    return user


username = ask_user()
user = delete_user(username)
if user:
    print("Deleted user")
else:
    print("User does not exist")
