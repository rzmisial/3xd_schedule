import os

_SECRET_KEY_FILENAME = "secret_key"


def get_secret_key():
    if secret_key_file_exists():
        secret_key = read_secret_key_file()
    else:
        secret_key = generate_secret_key()
        write_secret_key_file(secret_key)
    return secret_key


def generate_secret_key():
    return os.urandom(16)
    

def secret_key_file_exists():
    return os.path.isfile(_SECRET_KEY_FILENAME)


def read_secret_key_file():
    with open(_SECRET_KEY_FILENAME, 'rb') as f:
        return f.read()


def write_secret_key_file(key):
    with open(_SECRET_KEY_FILENAME, 'wb') as f:
        return f.write(key)
