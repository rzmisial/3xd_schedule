# Based on https://stackoverflow.com/questions/9594125/salt-and-hash-a-password-in-python?fbclid=IwAR0BsCu_JvcW6t5k0JTuEbqjdbigRy2s49ZFnSb4IWGuTvWklOMJc-Qf3k8

import os
import hashlib
import hmac

SALT_SIZE = 16
HASH_SIZE = 32

def hash_password(password):
    """
    Hash the provided password with a randomly-generated salt and return the
    salt and hash to store in the database.
    """
    salt = os.urandom(SALT_SIZE)
    pw_hash = hashlib.pbkdf2_hmac('sha256', password.encode(), salt, 100000)
    return salt, pw_hash
    
def is_correct_password(salt, pw_hash, password):
    """
    Given a previously-stored salt and hash, and a password provided by a user
    trying to log in, check whether the password is correct.
    """
    return hmac.compare_digest(
        pw_hash,
        hashlib.pbkdf2_hmac('sha256', password.encode(), salt, 100000)
    )
