import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from .model import Model, User

_DATABASE_FILENAME = 'database.db'


def _get_engine():
    if _get_engine.engine is None:
        _get_engine.engine = create_engine('sqlite:///' + _DATABASE_FILENAME)
    return _get_engine.engine


_get_engine.engine = None


def exists():
    return os.path.isfile(_DATABASE_FILENAME)


def create():
    Model.metadata.create_all(_get_engine())


def connect():
    return _get_engine().connect()


Session = sessionmaker(bind=_get_engine())


def add(obj):
    s = Session()
    try:
        s.add(obj)
        s.commit()
        obj_id = obj.id
        return str(obj_id)
    except:
        s.rollback()
        raise
    finally:
        s.close()


def find_all(model):
    result = [obj.serialize() for obj in Session().query(model).all()]
    return result


def delete_id(model, obj_id):
    s = Session()
    try:
        obj = s.query(model).filter_by(id=obj_id).one()
        s.delete(obj)
        s.commit()
        return str(obj_id)
    except:
        s.rollback()
        raise
    finally:
        s.close()


def find_id(model, obj_id):
    obj = Session().query(model).get(obj_id)
    return obj.serialize()


def request_model(model, obj_id):
    s = Session()
    try:
        obj = s.query(model).filter_by(id=obj_id).one()
        return obj, s
    except:
        s.close()
        raise


def get_session():
    return Session()


def find_id_model(session, obj_id, model):
    return session.query(model).get(obj_id)


def make_session_related_changes(session):
    session.commit()
    session.close()


def find_user_by_login(login):
    s = Session()
    try:
        user = s.query(User).filter_by(login=login).one_or_none()
        return user
    finally:
        s.close()
        