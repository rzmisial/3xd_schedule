from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship, backref

from .model import Model


class Node(Model):
    __tablename__ = 'node'
    id = Column(Integer, primary_key=True)
    name = Column(String(80))
    parent_id = Column(Integer, ForeignKey('node.id', ondelete='cascade'))
    children = relationship('Node', backref=backref('parent', remote_side=[id]), cascade='all, delete')
    classes = relationship('Class', backref='node', cascade='all, delete')

    def serialize(self):
        return {"id": self.id,
                "name": self.name,
                "parent_id": self.parent_id
                }
