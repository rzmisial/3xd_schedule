from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from .model import Model
from .class_classroom import ClassClassroom


class Classroom(Model):
    __tablename__ = 'classroom'
    id = Column(Integer, primary_key=True)
    name = Column(String(80))
    building_id = Column(Integer, ForeignKey('building.id'))
    classes = relationship('Class', secondary=ClassClassroom, backref='classrooms')

    def serialize(self):
        return {"id": self.id,
                "name": self.name,
                "building_id": self.building_id
                }
