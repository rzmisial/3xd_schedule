from sqlalchemy import Column, Integer, String, Binary

from .model import Model


class User(Model):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    login = Column(String(32), unique=True)
    hashed_password = Column(Binary(32))
    salt = Column(Binary(16))

    def serialize(self):
        return {"id": self.id,
                "login": self.name,
                "hashed_password": self.hashed_password,
                "salt": self.salt,
                }
