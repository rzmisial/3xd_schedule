from sqlalchemy import Column, ForeignKey, Integer, String, Time

from .model import Model


class Class(Model):
    __tablename__ = 'class'
    id = Column(Integer, primary_key=True)
    name = Column(String(80))
    abbreviation = Column(String(10))
    classtype = Column(String(32))
    teachers = Column(String(80))
    day = Column(Integer)
    time = Column(Time)
    duration = Column(Integer)  # in minutes
    node_id = Column(Integer, ForeignKey('node.id', ondelete='cascade'))

    def serialize(self):
        return {"id": self.id,
                "name": self.name,
                "abbreviation": self.abbreviation,
                "classtype": self.classtype,
                "teachers": self.teachers,
                "day": self.day,
                "time": str(self.time),
                "duration": self.duration,
                "node_id": self.node_id
                }
