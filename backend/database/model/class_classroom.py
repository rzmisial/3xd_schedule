from sqlalchemy import Column, ForeignKey, Integer, Table

from .model import Model

ClassClassroom = Table('class_classroom', Model.metadata,
                       Column('class_id', Integer, ForeignKey('class.id', ondelete='cascade')),
                       Column('classroom_id', Integer, ForeignKey('classroom.id', ondelete='cascade'))
                       )
