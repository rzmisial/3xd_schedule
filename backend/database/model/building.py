from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.orm import relationship

from .model import Model


class Building(Model):
    __tablename__ = 'building'
    id = Column(Integer, primary_key=True)
    name = Column(String(80))
    longitude = Column(Float)
    latitude = Column(Float)
    classrooms = relationship('Classroom', backref='building')

    def serialize(self):
        return {"id": self.id,
                "name": self.name,
                "longitude": self.longitude,
                "latitude": self.latitude
                }
