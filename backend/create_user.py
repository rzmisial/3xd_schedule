import sys
from getpass import getpass

from crypto.hashing import hash_password
from database import database
from database.model import User


def ask_user():
    print("Username:")
    return sys.stdin.readline().rstrip('\n')

def ask_password():
    password = getpass()
    return password

def create_user(username, password):
    salt, hashed_password = hash_password(password)
    user = User(login = username, hashed_password = hashed_password, salt = salt)
    database.add(user)

username = ask_user()
password = ask_password()
create_user(username, password)
