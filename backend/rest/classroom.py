from flask import request
from flask.json import jsonify
from flask_jwt_extended import jwt_required

from database import database
from database.model import Classroom
from .common import json_created, json_deleted


def apply_routing(app):
    app.route('/classroom', methods=["POST"])(create_classroom)
    app.route('/classroom/<int:find_ID>', methods=['GET'])(get_classroom)
    app.route('/classroom', methods=["GET"])(get_all_classrooms)
    app.route('/classroom/<int:delete_ID>', methods=['DELETE'])(delete_classroom)
    app.route('/building/<int:find_ID>/classroom', methods=['GET'])(get_all_building_related_classrooms)
    app.route('/class/<int:find_ID>/classroom', methods=["GET"])(get_all_class_related_classrooms)


def get_all_class_related_classrooms(find_ID):
    session = database.get_session()
    result = [obj.serialize() for obj in session.query(Classroom).filter(Classroom.classes.any(id=find_ID)).all()]
    database.make_session_related_changes(session)
    return jsonify(result)


def get_all_building_related_classrooms(find_ID):
    session = database.get_session()
    result = [obj.serialize() for obj in
              session.query(Classroom).filter_by(building_id=find_ID).all()]
    database.make_session_related_changes(session)
    return jsonify(result)


@jwt_required
def delete_classroom(delete_ID):
    return json_deleted(Classroom, delete_ID)


@jwt_required
def create_classroom():
    classroom = Classroom(name=request.json['name'], building_id=request.json['building_id'])
    obj_id = database.add(classroom)
    return json_created(obj_id)


def get_classroom(find_ID):
    return jsonify(database.find_id(Classroom, find_ID))


def get_all_classrooms():
    return jsonify(database.find_all(Classroom))
