from . import building, classroom, class_, node, user


def apply_routing(app):
    building.apply_routing(app)
    classroom.apply_routing(app)
    class_.apply_routing(app)
    node.apply_routing(app)
    user.apply_routing(app)
