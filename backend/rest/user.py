from flask import request
from flask.json import jsonify
from flask_jwt_extended import create_access_token

from crypto.hashing import is_correct_password
from database import database
from .common import json_created, json_deleted


def apply_routing(app):
    app.route('/user/login', methods=['POST'])(authenticate_user)


def authenticate_user():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)
    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400
    
    user = database.find_user_by_login(username)
    authenticated = user and is_correct_password(user.salt, user.hashed_password, password)
    if authenticated:
        access_token = create_access_token(identity=username)
        return jsonify(access_token=access_token), 200
    else:
        return jsonify({"msg": "Bad username or password"}), 401
