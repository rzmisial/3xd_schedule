from flask import request
from flask.json import jsonify
from flask_jwt_extended import jwt_required

from database import database
from database.model import Building
from .common import json_created, json_deleted, json_edited, json_edited_error


def apply_routing(app):
    app.route('/building', methods=["POST"])(create_building)
    app.route('/building/<int:delete_ID>', methods=['DELETE'])(delete_building)
    app.route('/building/<int:find_ID>', methods=['GET'])(get_building)
    app.route('/building/<int:update_ID>', methods=['PUT'])(update_building)
    app.route('/building', methods=["GET"])(get_all_buildings)

    
@jwt_required
def create_building():
    building = Building(name=request.json['name'],
                        longitude=request.json['longitude'],
                        latitude=request.json['latitude'])
    obj_id = database.add(building)
    return json_created(obj_id)


@jwt_required
def update_building(update_ID):
    try:
        building, session = database.request_model(Building, update_ID)
        building.name = request.json['name']
        building.longitude = request.json['longitude']
        building.latitude = request.json['latitude']
        database.make_session_related_changes(session)
        return json_edited(update_ID)
    except:
        return json_edited_error(update_ID)


def get_building(find_ID):
    return jsonify(database.find_id(Building, find_ID))


def get_all_buildings():
    return jsonify(database.find_all(Building))


@jwt_required
def delete_building(delete_ID):
    return json_deleted(Building, delete_ID)
