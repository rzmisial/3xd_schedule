from flask import request
from flask.json import jsonify
from flask_jwt_extended import jwt_required

from database import database
from database.model import Node
from rest.common import json_created, json_edited, json_edited_error, json_deleted


def apply_routing(app):
    app.route('/node/<int:find_ID>', methods=['GET'])(get_node)
    app.route('/node/<int:delete_ID>', methods=['DELETE'])(delete_node)
    app.route('/node/<int:update_ID>', methods=['PUT'])(update_node)
    app.route('/node', methods=['GET'])(get_all_nodes)
    app.route('/node', methods=["POST"])(create_node)
    # app.route('/node/<int:node_ID>/class/<int:class_ID>', methods=["PATCH"])(associate_node_class)
    app.route('/node/<int:node_ID>/node/<int:node_child_ID>', methods=["PATCH"])(associate_node_child_node)


@jwt_required
def delete_node(delete_ID):
    return json_deleted(Node, delete_ID)


@jwt_required
def update_node(update_ID):
    try:
        node, session = database.request_model(Node, update_ID)
        node.name = request.json['name']
        database.make_session_related_changes(session)
        return json_edited(update_ID)
    except:
        return json_edited_error(update_ID)


def get_all_nodes():
    return jsonify(database.find_all(Node))


def get_node(find_ID):
    return jsonify(database.find_id(Node, find_ID))


@jwt_required
def associate_node_child_node(node_ID, node_child_ID):
    try:
        session = database.get_session()
        node = database.find_id_model(session, node_ID, Node)
        node_child = database.find_id_model(session, node_child_ID, Node)
        node.children.append(node_child)
        database.make_session_related_changes(session)
        return jsonify(
            status="ok",
            msg="Node with id: " + str(node_ID) + " has been associated with child node id: " + str(node_child_ID))
    except:
        return jsonify(
            status="error",
            msg="Node with id: " + str(node_ID) + " cannot be associated with child node: " + str(node_child_ID))


# def associate_node_class(node_ID, class_ID):
#     try:
#         session = database.get_session()
#         node = database.find_id(session, node_ID, Node)
#         class_ = database.find_id(session, class_ID, Class)
#         node.classes.append(class_)
#         database.make_session_related_changes(session)
#         return jsonify(
#             status="ok",
#             msg="Node with id: " + str(node_ID) + " has been associated with Class id: " + str(class_ID))
#     except:
#         return jsonify(
#             status="error",
#             msg="Node with id: " + str(node_ID) + " cannot be associated with Class id: " + str(class_ID))


@jwt_required
def create_node():
    node = Node(name=request.json['name'])
    obj_id = database.add(node)
    return json_created(obj_id)
