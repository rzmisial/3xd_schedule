from datetime import datetime

from flask import request
from flask.json import jsonify
from flask_jwt_extended import jwt_required

from database import database
from database.model import Class, Classroom
from .common import json_created, json_deleted, json_edited, json_edited_error


def apply_routing(app):
    app.route('/node/<int:find_ID>/class', methods=['GET'])(get_all_node_related_class)
    app.route('/class/<int:update_ID>', methods=['PUT'])(update_class)
    app.route('/class', methods=["POST"])(create_class)
    app.route('/class/<int:delete_ID>', methods=['DELETE'])(delete_class)
    app.route('/class', methods=["GET"])(get_all_classes)
    app.route('/classroom/<int:find_ID>/class', methods=["GET"])(get_all_classroom_related_classes)
    app.route('/class/<int:find_ID>', methods=['GET'])(get_class)
    app.route('/class/<int:class_ID>/classroom/<int:classroom_ID>', methods=['PATCH'])(associate_class_classroom)
    app.route('/dissociate/class/<int:class_ID>/classroom/<int:classroom_ID>', methods=['PATCH'])(
        dissociate_class_classroom)


def get_all_node_related_class(find_ID):
    session = database.get_session()
    result = [obj.serialize() for obj in session.query(Class).filter_by(node_id=find_ID).all()]
    database.make_session_related_changes(session)
    return jsonify(result)


@jwt_required
def update_class(update_ID):
    try:
        class_, session = database.request_model(Class, update_ID)
        class_.name = request.json['name']
        class_.abbreviation = request.json['abbreviation']
        class_.teachers = request.json['teachers']
        class_.duration = request.json['duration']
        class_.classtype = request.json['classtype']
        class_.day = request.json['day']
        class_.time = datetime.strptime(request.json['time'], '%H:%M:%S').time()
        class_.node_id = request.json['node_id']
        database.make_session_related_changes(session)
        return json_edited(update_ID)
    except:
        return json_edited_error(update_ID)

    
@jwt_required
def delete_class(delete_ID):
    return json_deleted(Class, delete_ID)


def get_all_classroom_related_classes(find_ID):
    session = database.get_session()
    result = [obj.serialize() for obj in session.query(Class).filter(Class.classrooms.any(id=find_ID)).all()]
    database.make_session_related_changes(session)
    return jsonify(result)


def get_class(find_ID):
    return jsonify(database.find_id(Class, find_ID))


def get_all_classes():
    return jsonify(database.find_all(Class))


@jwt_required
def create_class():
    class_ = Class(name=request.json['name'], abbreviation=request.json['abbreviation'],
                   teachers=request.json['teachers'], duration=request.json['duration'],
                   classtype=request.json['classtype'], day=request.json['day'],
                   node_id=request.json['node_id'],
                   time=datetime.strptime(request.json['time'], '%H:%M:%S').time())
    obj_id = database.add(class_)
    return json_created(obj_id)


@jwt_required
def associate_class_classroom(class_ID, classroom_ID):
    try:
        session = database.get_session()
        class_ = database.find_id_model(session, class_ID, Class)
        classroom = database.find_id_model(session, classroom_ID, Classroom)
        classroom.classes.append(class_)
        database.make_session_related_changes(session)
        return jsonify(
            status="ok",
            msg="Class with id: " + str(class_ID) + " has been associated with Classrom id: " + str(classroom_ID))
    except:
        return jsonify(
            status="error",
            msg="Class with id: " + str(class_ID) + " cannot be associated with Classrom id: " + str(classroom_ID))


@jwt_required
def dissociate_class_classroom(class_ID, classroom_ID):
    try:
        session = database.get_session()
        class_ = database.find_id_model(session, class_ID, Class)
        classroom = database.find_id_model(session, classroom_ID, Classroom)
        classroom.classes.remove(class_)
        database.make_session_related_changes(session)
        return jsonify(
            status="ok",
            msg="Class with id: " + str(class_ID) + " has been disassociated with Classrom id: " + str(classroom_ID))
    except:
        return jsonify(
            status="error",
            msg="Class with id: " + str(class_ID) + " cannot be disassociated with Classrom id: " + str(classroom_ID))
