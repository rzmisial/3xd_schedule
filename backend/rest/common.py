from flask.json import jsonify

from database import database


def json_created(obj_id):
    return jsonify(id=obj_id, msg="Created item with id: " + obj_id)


def json_deleted(model, obj_id):
    try:
        database.delete_id(model, obj_id)
        return jsonify(id=obj_id, msg="Deleted item with id: " + str(obj_id))
    except:
        return jsonify(error=404, msg="Item with id: " + str(obj_id) + " does not exist in DB"), 404


def json_edited(obj_id):
    return jsonify(id=obj_id, msg="item with id: " + str(obj_id) + " has been edited")


def json_edited_error(obj_id):
    return jsonify(error=404, msg="Item with id: " + str(obj_id) + " does not exist in DB"), 404
