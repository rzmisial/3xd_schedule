from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager

from database import database
from rest import apply_routing
from crypto import get_secret_key


app = Flask(__name__)
app.secret_key = get_secret_key()

JWTManager(app)

CORS(app)

if not database.exists():
    database.create()
database.connect()

apply_routing(app)
