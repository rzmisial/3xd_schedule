// User management

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  public updateUser(nodeId: number){
    // TODO: request backend user update (eg. email)
  }
}
