import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private httpClient: HttpClient) { }

  getAllLocations(){
    const locationEndpoint = environment.apiBaseUrl + '/building';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': environment.apiBaseUrl,
      })
    };
    return this.httpClient.get(locationEndpoint, httpOptions);
  }

  ///building/<int:find_ID>/classroom
  getClassroomsForLocations(buildingId: number){
    const locationEndpointStart = environment.apiBaseUrl + '/building/';
    const locationEndpointEnd = '/classroom';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': environment.apiBaseUrl,
      })
    };
    return this.httpClient.get(locationEndpointStart + buildingId + locationEndpointEnd, httpOptions);
  }

}
