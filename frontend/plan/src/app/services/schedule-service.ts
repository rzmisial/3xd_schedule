// Schedules management

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { RawClass } from '../model/rawClass';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService{

  constructor(private httpClient: HttpClient) { }

  public getScheduleLocations(){
    const locationEndpoint = environment.apiBaseUrl + '/building';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': environment.apiBaseUrl,
      })
    };
    let locations: any[]
    return this.httpClient.get(locationEndpoint, httpOptions);
  }

  public getScheduleRawClasses(nodeId: number){
    const classEndpoint = environment.apiBaseUrl + '/node/' + nodeId + '/class';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': environment.apiBaseUrl,
      })
    };
    let locations: any[]
    return this.httpClient.get(classEndpoint, httpOptions);
  }

  public getScheduleClassrooms(c: RawClass){
    const classroomEndpointStart = environment.apiBaseUrl + '/class/';
    const classroomEndpointEnd = '/classroom';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': environment.apiBaseUrl,
      })
    };
    let locations: any[]
    return this.httpClient.get(classroomEndpointStart + c.id +classroomEndpointEnd, httpOptions);
  }

  public postSchedule(classes: RawClass){
    
    const classEndpoint = environment.apiBaseUrl + '/class';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': environment.apiBaseUrl,
      })
    };
    return this.httpClient.post(classEndpoint, classes, httpOptions);
  }

  public associateClassroomSchedule(classId: number, classroomId: number){
    
    const endpoint = environment.apiBaseUrl + '/class/' + classId + '/classroom/' + classroomId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': environment.apiBaseUrl,
      })
    };
    return this.httpClient.patch(endpoint, httpOptions);
  }

  public dissociateClassroomSchedule(classId: number, classroomId: number){

    const endpoint = environment.apiBaseUrl + '/dissociate/class/' + classId + '/classroom/' + classroomId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': environment.apiBaseUrl,
      })
    };
    return this.httpClient.patch(endpoint, httpOptions);
  }

  public updateSchedule(classId: number, classes: RawClass){

    const classEndpoint = environment.apiBaseUrl + '/class/' + classId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': environment.apiBaseUrl,
      })
    };

    return this.httpClient.put(classEndpoint, classes, httpOptions);
  }

  public deleteSchedule(classId: number){

    const classEndpoint = environment.apiBaseUrl + '/class/' + classId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': environment.apiBaseUrl,
      })
    };

    return this.httpClient.delete(classEndpoint, httpOptions);
  }

}
