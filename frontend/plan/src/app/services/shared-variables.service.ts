import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedVariables {
  // Here comes all global variables 
  public currentNode: number;
}
