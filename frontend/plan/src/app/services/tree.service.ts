// Tree management 

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, from } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TreeService {

  constructor(private httpClient: HttpClient) { }

  public getNodes(): Observable<any> {
    const endpoint = environment.apiBaseUrl + '/node';
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin': environment.apiBaseUrl,
    })
    };

    return this.httpClient.get(endpoint, httpOptions);
  }

  public getNode(id: number): Observable<any> {

    const endpoint = environment.apiBaseUrl + '/node/' + id;
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin': environment.apiBaseUrl,
    })
    };

    return this.httpClient.get(endpoint, httpOptions);
  }

}
