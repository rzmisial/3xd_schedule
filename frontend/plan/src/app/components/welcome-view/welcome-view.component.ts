import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome-view',
  templateUrl: './welcome-view.component.html',
  styleUrls: ['./welcome-view.component.sass']
})
export class WelcomeViewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
