import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Class } from '../../../model/class';
import { ClassType } from '../../../model/class.type';
import { AddDialogComponent } from '../../add-dialog/add-dialog.component';
import { AuthenticationService } from 'src/app/services/authentication-service';

@Component({
  selector: 'app-taskbar',
  templateUrl: './taskbar.component.html',
  styleUrls: ['./taskbar.component.sass']
})


export class TaskbarComponent implements OnInit {

  private loggedIn: String;

  constructor(public dialog: MatDialog, private authenticationService: AuthenticationService) {
    this.loggedIn = localStorage.getItem('currentUser');
  }

  
  ngOnInit() {
  }
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    this.dialog.open(AddDialogComponent, dialogConfig);
  }

  logOut(){
    this.authenticationService.logout();
    window.location.reload();
  }

}
