import { CollectionViewer, SelectionChange } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, Injectable, OnInit, ɵclearOverrides, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject, merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { TreeNode } from '../../../model/treeNode';
import { TreeService } from '../../../services/tree.service';

import * as _ from 'lodash';


export class DynamicFlatNode {
  constructor(
    public id: number,
    public item: string,
    public level = 1,
    public expandable = false,
    public parent: number,
    public children: number[],
    public isLoading = false) { }
}


//  TEMPORARY BEFORE READING FROM DATABASE // 

@Injectable()
export class DynamicDatabase { // NEW SERVICE IN CONSTRUCTOR


  // 1st Reading from JSON file (HTTP client), logical changing into presented structure. Changing from [string string] this section in here ( Map<string, string[]>)


  private dataMap: Map<number, number[]>;       // DataMap (parent.id (map) [children.id])
  private rootLevelNodes: number[];             // RootLevel Nodes 
  private childParent: Map<number, number>;     // Children - Parent map [child.id (map) parent.id]
  private items: TreeNode[];                   // Current list of items (checked each time get children is triggered)


  constructor(nodes: TreeNode[]) {

    this.items = nodes;
    this.createDataMap(this.items);

  }


  createDataMap(items: TreeNode[]): void {    // Changing the structure of the tree given from a file to the map 

    this.dataMap = new Map<number, number[]>([]);
    this.childParent = new Map<number, number>([]);
    this.rootLevelNodes = [];
    var a: number = 0;

    var parents: number[] = [];
    var children: number[][] = [];
    var ids: number[] = [];

    for (var item of items) {

      this.childParent.set(item.id, item.parent_id);

      if (!parents.includes(item.parent_id) && item.parent_id != null) {
        parents.push(item.parent_id)
        ids.push(a);
        children.push([]);
        a = a + 1;

      }
      if (item.parent_id == null) {
        this.rootLevelNodes.push(item.id);
      }
    }
    for (var id of ids) {
      for (var item of items) {
        if (item.parent_id == parents[id]) {
          children[id].push(item.id);
        }
      }
    }
    for (var id of ids) {
      this.dataMap.set(parents[id], children[id]);
    }
  }

  initialData(): DynamicFlatNode[] {
    return this.rootLevelNodes.map(id =>
      new DynamicFlatNode(id, this.getName(id), 0, this.isExpandable(id),
        this.getParent(id), this.getChildren(id)));   // Initial Data to be sent
  }

  getChildren(nodeId: number): number[] | undefined {   // Get children of node
    return this.dataMap.get(nodeId);
  }

  isExpandable(nodeId: number): boolean {   // If is expandable (has children)
    return this.dataMap.has(nodeId);
  }

  getParent(nodeId: number): number | undefined {  // Get Parents id
    for (var item of this.childParent)
      if (nodeId == item[0]) return item[1];
    return null;
  }

  getName(nodeId: number): string {  // Get name by id
    for (var item of this.items)
      if (nodeId == item.id) return item.name;
  }
}

@Injectable()
export class DynamicDataSource {

  dataChange = new BehaviorSubject<DynamicFlatNode[]>([]);   // New behavior subject for instance of the data changes

  get data(): DynamicFlatNode[] { return this.dataChange.value; }  // Get and set dataChange
  set data(value: DynamicFlatNode[]) {
    this._treeControl.dataNodes = value;
    this.dataChange.next(value);
  }

  constructor(
    private _treeControl: FlatTreeControl<DynamicFlatNode>,
    private _database: DynamicDatabase) { }

  connect(collectionViewer: CollectionViewer): Observable<DynamicFlatNode[]> {     // Handle changes in data -> send to handleTreeControl function
    this._treeControl.expansionModel.onChange.subscribe(change => {               // Uses collection viewer 
      if ((change as SelectionChange<DynamicFlatNode>).added ||
        (change as SelectionChange<DynamicFlatNode>).removed) {
        this.handleTreeControl(change as SelectionChange<DynamicFlatNode>);
      }
    });

    return merge(collectionViewer.viewChange, this.dataChange).pipe(map(() => this.data));
  }

  /** Handle expand/collapse behaviors */
  handleTreeControl(change: SelectionChange<DynamicFlatNode>) {                       // Handle changes sending to display handle
    if (change.added) {
      change.added.forEach(node => this.toggleNode(node, true));
    }
    if (change.removed) {
      change.removed.slice().reverse().forEach(node => this.toggleNode(node, false));
    }
  }

  /**
   * Toggle the node, remove from display list
   */
  toggleNode(node: DynamicFlatNode, expand: boolean) {                                  // Handle changes in display 
    const children = this._database.getChildren(node.id);                               // 2. BEFORE THAT, CHECKING THE DATABASE 
    const index = this.data.indexOf(node);
    if (!children || index < 0) { // If no children, or cannot find the node, no op
      return;
    }

    node.isLoading = true; // New instance is loading

    setTimeout(() => {
      if (expand) {
        const nodes = children.map(id =>
          new DynamicFlatNode(id, this._database.getName(id), node.level + 1, this._database.isExpandable(id),
            this._database.getParent(id), this._database.getChildren(id)));
        // If new -new dynamic flat node
        this.data.splice(index + 1, 0, ...nodes);
      } else {                                                                          // If removed, change the level of 
        let count = 0;                                                                  // apropriate leaves
        for (let i = index + 1; i < this.data.length
          && this.data[i].level > node.level; i++ , count++) { }
        this.data.splice(index + 1, count);
      }

      // notify the change
      this.dataChange.next(this.data);
      node.isLoading = false;
    }, 50); // Loading waiting time
  }

}


@Component({
  selector: 'app-navigator',
  templateUrl: './navigator.component.html',
  styleUrls: ['./navigator.component.sass'],
  providers: [DynamicDatabase]
})
export class NavigatorComponent implements OnInit {

  constructor(private treeService: TreeService, private cd: ChangeDetectorRef) {}

  database: DynamicDatabase;
  treeControl: FlatTreeControl<DynamicFlatNode>;                                               // List of HTML refs
  dataSource: DynamicDataSource;

  getId = (node: DynamicFlatNode) => node.id;                                                 // node into getters
  getName = (node: DynamicFlatNode) => node.item;
  getLevel = (node: DynamicFlatNode) => node.level;
  getParent = (node: DynamicFlatNode) => node.parent;
  getChildren = (node: DynamicFlatNode) => node.children;
  isExpandable = (node: DynamicFlatNode) => node.expandable;

  hasChild = (_: number, _nodeData: DynamicFlatNode) => _nodeData.expandable;

  test = false;

  ngOnInit() {
    this.treeService.getNodes().subscribe( n => {
      this.database = new DynamicDatabase(n);
      this.treeControl = new FlatTreeControl<DynamicFlatNode>(this.getLevel, this.isExpandable); // New tree control
      this.dataSource = new DynamicDataSource(this.treeControl, this.database);                       // New database         
      this.dataSource.data = this.database.initialData();
      this.test = true;
      this.cd.markForCheck();
    })
    
  }
}
