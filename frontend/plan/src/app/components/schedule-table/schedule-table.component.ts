import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { Class } from '../../model/class';
import { ClassType } from 'src/app/model/class.type';
import { ActivatedRoute } from '@angular/router';
import { MapComponent } from '../map/map.component';
import { SharedVariables } from 'src/app/services/shared-variables.service';
import { ScheduleService } from 'src/app/services/schedule-service';
import { RawClass } from 'src/app/model/rawClass';
import { Observable, forkJoin } from 'rxjs';
import { LocationService } from 'src/app/services/location.service';
import { AddDialogComponent } from '../add-dialog/add-dialog.component';
import { TreeService } from 'src/app/services/tree.service';



let seventh = 100 / 7;

@Component({
  selector: 'app-schedule-table',
  templateUrl: './schedule-table.component.html',
  styleUrls: ['./schedule-table.component.sass']
})
export class ScheduleTableComponent implements OnInit, OnDestroy {

  private title="";
  private startHour = 8;
  private endHour = 16;
  private scheduleLength: number;     // schedule length in hours

  private minScheduleHeight = 500;    // min schedule height in pixels
  private scheduleHeight: String;     // final schedule heigth as a string
  // if IDE says it's not used, IGNORE! (it is)

  private classes: Class[];
  private classesExtended = [];       // classes with extra info (position on canvas, height of the block)
  private legend = [];
  private gridCells = [];             // all cells making up the grey grid
  private hourCells = [];             // all cells making up the hour column on the left

  nodeId: number;
  private sub: any;

  constructor(public dialog: MatDialog,
        private route: ActivatedRoute,
        private globals: SharedVariables,
        private scheduleService: ScheduleService,
        private locationService: LocationService,
        private treeService: TreeService,
        private cd: ChangeDetectorRef) {
        }

  private initHours() {
    // Create the list of hours for the hour column displayed on the left
    this.classes.forEach(element => {
      let endHour = Math.ceil(element.hour + element.minute / 60 + element.duration / 60);
      if (endHour > this.endHour)
        this.endHour = endHour;
    });
  }

  private calculateScheduleLength() {
    let calculatedScheduleHeight = (500 / 8) * (this.endHour - this.startHour);

    if (calculatedScheduleHeight > this.minScheduleHeight)
      this.scheduleHeight = calculatedScheduleHeight + 'px';
    else
      this.scheduleHeight = this.minScheduleHeight + 'px';

    this.scheduleLength = this.endHour - this.startHour;
  }

  private initClassesAndLegend() {

    this.classes.forEach(element => {
      let dayOfWeekNo = element.day;
      if (dayOfWeekNo === 0)
        dayOfWeekNo = 7

      this.classesExtended.push({
        ...element,
        xPos: ((dayOfWeekNo - 1) * seventh) + "%",
        yPos: (((element.hour + element.minute / 60) - this.startHour) * 100 / this.scheduleLength) + "%",
        height: (element.duration / 60 / this.scheduleLength) * 100 + "%"
      });

      let dayOfWeek: String;

      switch (dayOfWeekNo) {
        case 1: dayOfWeek = 'Poniedziałek'; break;
        case 2: dayOfWeek = 'Wtorek'; break;
        case 3: dayOfWeek = 'Środa'; break;
        case 4: dayOfWeek = 'Czwartek'; break;
        case 5: dayOfWeek = 'Piątek'; break;
        case 6: dayOfWeek = 'Sobota'; break;
        case 7: dayOfWeek = 'Niedziela'; break;
        default: console.log("Day of week error. Day of week is: " + dayOfWeekNo)
      }

      let classEndHour = element.hour + Math.floor(element.duration / 60);
      let classEndMinute = (element.minute + element.duration)%60;
      this.legend.push({
        name: element.abbreviation,
        time: dayOfWeek + ' ' + element.hour + ':' + element.minute.toLocaleString(undefined, { minimumIntegerDigits: 2 }) +
          ' - ' + classEndHour + ':' + classEndMinute.toLocaleString(undefined, { minimumIntegerDigits: 2 }),
        fullName: element.name
      });
    });

    this.legend.sort((a, b) => (a.name > b.name) ? 1 : -1)
  }

  private initGrid() {
    let days = Array.from(Array(7).keys())
    let hours = Array.from(Array(this.endHour - this.startHour).keys());

    let cellHeight = (100 / (this.endHour - this.startHour));

    for (let d of days) {
      for (let h of hours) {
        this.gridCells.push({ xPos: d * seventh + '%', yPos: h * cellHeight + '%', height: cellHeight + '%' })
      }
    }

    for (let h of hours) {
      this.hourCells.push({ xPos: '0%', yPos: h * cellHeight + '%', height: cellHeight + '%', text: 8 + h + '-' + (9 + h) })
    }
  }

  private loadData(nodeId: number){

    this.treeService.getNode(nodeId).subscribe(res => this.title = res.name);

    this.locationService.getAllLocations().subscribe(locs => {
      this.classes = [];
      let locations = locs as any[];

      this.scheduleService.getScheduleRawClasses(nodeId).subscribe(rcl => {
        let rawClasses = rcl as RawClass[];
        
        let classroomResults: Observable<any>[] = [];
        for(let c of rawClasses){
          classroomResults.push(this.scheduleService.getScheduleClassrooms(c));
        }

        forkJoin(classroomResults).subscribe(results => {
          

          for(let i = 0; i < classroomResults.length; i++){

            let classrooms: any[] = [];
            
            let classroomNames = [];
            for(let clsRoom of results[i] as any[]){
              classrooms.push(clsRoom);
              classroomNames.push(clsRoom.name)
            }
            
            let timeSeparated = rawClasses[i].time.split(':');
            let teachersSeparated = rawClasses[i].teachers.split(';');
            let hour = parseInt(timeSeparated[0]);
            let minute = parseInt(timeSeparated[1]);
            let location = locations.find(l => {if(l.id == classrooms[0].building_id) return l});
            
            let cl: Class = {
              id: rawClasses[i].id,
              name: rawClasses[i].name,
              abbreviation: rawClasses[i].abbreviation,
              day: rawClasses[i].day,
              hour: hour,
              minute: minute,
              duration: rawClasses[i].duration,
              teachers: teachersSeparated,
              nodeId: rawClasses[i].node_id,
              buildingName: location.name,
              classtype: ClassType[rawClasses[i].classtype.toString()],
              classrooms: classroomNames,
              location: {longitude: location.longitude, latitude: location.latitude,}
            };
            
            this.classes.push(cl);
          }
          
          if(this.classes != undefined){
            this.initHours();
            this.calculateScheduleLength();
            this.initClassesAndLegend();
            this.initGrid();
          }
        })

      })
    });
  }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.classes = [];
      this.classesExtended = [];
      this.legend = [];
      this.gridCells = [];
      this.hourCells = [];
      this.startHour = 8;
      this.endHour = 16;

      this.nodeId = +params['nodeId']; // (+) converts string 'nodeId' to a number
      this.globals.currentNode = +this.nodeId;
      // Adds node as a global node.
      this.loadData(params['nodeId']);
    });

  }

  onClassClick(classExtended: any) {
    // open the map

    let loggedIn = localStorage.getItem('currentUser');
    const dialogConfig = new MatDialogConfig();

    if(loggedIn){
      
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.data = {class: classExtended}             // TO COMMENT - CHANGE BOX TYPE - TEMP
      this.dialog.open(AddDialogComponent, dialogConfig);
    } else {
      let location = classExtended.location;

      const dialogConfig = new MatDialogConfig();
      dialogConfig.autoFocus = true;
      dialogConfig.data = location;
      dialogConfig.width = "80%";
      dialogConfig.height = "80%";
      dialogConfig.minWidth = "400px";
      dialogConfig.minHeight = "400px"
      this.dialog.open(MapComponent, dialogConfig);
    }
  }
  
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
