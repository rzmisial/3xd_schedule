import { Component, OnInit, OnDestroy  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SharedVariables } from '../../services/shared-variables.service'

// Examplary class using URL params

@Component({
  selector: 'app-example1',
  templateUrl: './example1.component.html',
  styleUrls: ['./example1.component.sass']
})
export class Example1Component implements OnInit, OnDestroy {

  nodeId: number;
  private sub: any;

  constructor(private route: ActivatedRoute, private globals: SharedVariables) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.nodeId = +params['nodeId']; // (+) converts string 'nodeId' to a number
      this.globals.currentNode = +this.nodeId;
      // Adds node as a global node.
      // This is where you'd request the schedule from backend. All we're going to do here is using the value.
   });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
