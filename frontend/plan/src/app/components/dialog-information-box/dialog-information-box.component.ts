import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { MatDialogRef } from "@angular/material";

@Component({
  selector: 'app-dialog-information-box',
  templateUrl: './dialog-information-box.component.html',
  styleUrls: ['./dialog-information-box.component.sass']
})


export class DialogInformationBoxComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<DialogInformationBoxComponent>
  ) { }
  ngOnInit() {
  }


  confirmed() {
    let msg = new Array();
    this.dialogRef.close({ event: 'close', data: "confirmed" }); 

  }

  close() {
    this.dialogRef.close();
  }
}
