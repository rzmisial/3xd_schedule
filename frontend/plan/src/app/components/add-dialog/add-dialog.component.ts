import { Component, OnInit, Inject, Input, ChangeDetectorRef } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { MatDialog, MatDialogRef, MatDialogConfig } from "@angular/material";
import { SharedVariables } from '../../services/shared-variables.service';
import { ClassType, ClassTypeDialog } from '../../model/class.type';
import { Class } from '../../model/class';
import { Location } from '../../model/location';
import { DialogInformationBoxComponent } from '../dialog-information-box/dialog-information-box.component'
import { LocationService } from 'src/app/services/location.service';
import { ScheduleService } from 'src/app/services/schedule-service';
import { RawClass } from 'src/app/model/rawClass';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.sass']
})

// INFORMATION:
//
// Class id = -1 in case of new classes
//



export class AddDialogComponent implements OnInit {

  // Type of box
  typeOfBox: number = 0; // 0 - add, 1 - edit

  // Enum class Types for display
  public classDisplay = ClassTypeDialog;
  
  // Buildings
  public buildings: Array<String> = [];

  keys: string[];

  // Current node
  workNodeId: number;

  //Class input
  dataClass: Class;

  // Class output
  newClass: Class = new Class();
  newClassRaw: RawClass;

  //  Managed Input
  classId: number;
  classesName: String;
  abbreviation: String;
  typeOfClasses: ClassTypeDialog;
  day: number;
  beginTime: String;
  durationM: String;
  teachersM: String;
  buildingNameM: String;
  className: String;

  // Prepare managed Input 
  prepareManagedInput() {

    this.classId = this.dataClass.id;
    this.classesName = this.dataClass.name;
    this.abbreviation = this.dataClass.abbreviation;
    this.typeOfClasses = this.classTypeCD(this.dataClass.classtype);
    this.className = "";
    if (this.dataClass.classrooms != undefined) {
      for (let i = 0; i < this.dataClass.classrooms.length; i++) {
        this.className = this.className.concat(this.dataClass.classrooms[i].valueOf());
        if (i != this.dataClass.classrooms.length - 1) {
          this.className = this.className.concat(", ");
        }
      }
    }
    this.teachersM = "";
    if (this.dataClass.teachers != undefined) {
      for (let i = 0; i < this.dataClass.teachers.length; i++) {
        this.teachersM = this.teachersM.concat(this.dataClass.teachers[i].valueOf());
        if (i != this.dataClass.teachers.length - 1) {
          this.teachersM = this.teachersM.concat(", ");
        }
      }
    }

    this.buildingNameM = this.dataClass.buildingName;
    this.makePeriod(this.dataClass.duration)

    if (this.dataClass.day != undefined && this.dataClass.hour != undefined && this.dataClass.minute != undefined) {
      this.day = this.dataClass.day;
      if (this.dataClass.hour < 10 && this.dataClass.minute < 10) {
        this.beginTime = "0" + this.dataClass.hour.toString() + ":" + this.dataClass.minute.toString() + "0";
      }
      else if (this.dataClass.hour < 10) {
        this.beginTime = "0" + this.dataClass.hour.toString() + ":" + this.dataClass.minute.toString();
      }
      else if (this.dataClass.minute < 10) {
        this.beginTime = this.dataClass.hour.toString() + ":" + this.dataClass.minute.toString() + "0";
      }
      else {
        this.beginTime = this.dataClass.hour.toString() + ":" + this.dataClass.minute.toString();
      }
    }

  }

  hours: number;
  minutes: number;
  makePeriod(duration: number) {
    this.hours = Math.floor(duration / 60);
    this.minutes = duration % 60;
    if (this.hours < 10 && this.minutes < 10) {
      this.durationM = "0" + this.hours.toString() + ":" + this.minutes.toString() + "0";
    }
    else if (this.hours < 10) {
      this.durationM = "0" + this.hours.toString() + ":" + this.minutes.toString();
    }
    else if (this.minutes < 10) {
      this.durationM = this.hours.toString() + ":" + this.minutes.toString() + "0";
    }
    else {
      this.durationM = this.hours.toString() + ":" + this.minutes.toString();
    }

  }

  classTypeCD(tClass: ClassType): ClassTypeDialog {

    switch (tClass) {

      case (ClassType.blackboardClass): {
        return ClassTypeDialog.blackboardClass;
      }
      case (ClassType.laboratory): {
        return ClassTypeDialog.laboratory;
      }
      case (ClassType.lecture): {
        return ClassTypeDialog.lecture;
      }
      case (ClassType.other): {
        return ClassTypeDialog.other;
        break;
      }
      case (ClassType.project): {
        return ClassTypeDialog.project;
      }

    }

  }

  classTypeDC(tClass: ClassTypeDialog): ClassType {
    switch (tClass) {

      case (ClassTypeDialog.blackboardClass): {
        return ClassType.blackboardClass;
      }
      case (ClassTypeDialog.laboratory): {
        return ClassType.laboratory;
      }
      case (ClassTypeDialog.lecture): {
        return ClassType.lecture;
      }
      case (ClassTypeDialog.other): {
        return ClassType.other;
      }
      case (ClassTypeDialog.project): {
        return ClassType.project;
      }
    }
  }



  // Modified input
  typeOfClassesId: number;
  typeOfClassesN: ClassType;
  teachersList: String[];
  classNameList: String[];
  durationTemp: String[];
  durationMinutes: number;
  beginTimeM: String[];

  ngOnInit() {
  }

  // If input changes
  ngOnChange() {
    this.prepareManagedInput();
  }

  // Processing variables
  flagIfCorrect: boolean;
  msg = new Array();


  private locations: any[];
  private classrooms: any[];
  private chosenClassrooms: any[] = [];
  private curentLocation: any;

  // CONSTRUCTOR 
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private globals: SharedVariables,
    private dialogRef: MatDialogRef<AddDialogComponent>,
    public dialog: MatDialog,
    private locationService: LocationService,
    private scheduleService: ScheduleService) {
    this.workNodeId = this.globals.currentNode;
    locationService.getAllLocations().subscribe(locs => {

      this.locations = locs as any[];
      this.locations.forEach(l => this.buildings.push(l.name));
      if (data != null) {
        this.dataClass = data.class;

        this.prepareManagedInput();
        this.typeOfBox = 1;
      }
    })
  }


  close() {
    this.dialogRef.close();
  }

  save() {

    const dialogConfig = new MatDialogConfig();
    this.msg = new Array();
    this.flagIfCorrect = true;
    this.msg.push("");

    // ---------------------- VALIDATION ------------------------ // 

    // classesName
    if (this.classesName == undefined || this.classesName.trim() === "") {
      this.msg.push("Nie wpisano nazwy przedmiotu");
      this.flagIfCorrect = false;
    }

    // abbreviation
    if (this.abbreviation != undefined && this.abbreviation.trim() !== "") {

      if (this.abbreviation.length > 10) {
        this.msg.push("Skrót nie może być dłuższy niż 10 znaków");
        this.flagIfCorrect = false;
      }
      else if (this.abbreviation.indexOf(" ") != -1) {
        this.msg.push("Skrót nie może zawierać spacji");
        this.flagIfCorrect = false;
      }

    }
    else {
      this.msg.push("Nie wpisano skrótu przedmiotu");
      this.flagIfCorrect = false;
    }


    // typeOfClasses 
    if (this.typeOfClasses != undefined) {
      this.typeOfClassesN = this.classTypeDC(this.typeOfClasses);
    }
    else {
      this.msg.push("Nie zdefiniowano typu");
      this.flagIfCorrect = false;
    }

    // Building Name
    if (this.buildingNameM == undefined) {
      this.msg.push("Nie wybrano nazwy budynku");
      this.flagIfCorrect = false;
    } else {
      this.locations.find(l => { if (l.name == this.buildingNameM) this.curentLocation = l });

      if (this.curentLocation == undefined) {
        this.msg.push("Wystąpił nieoczekiwany błąd. Spróbuj ponownie później");
        this.flagIfCorrect = false;
      }
    }


    // Classrooms names 
    if (this.className != undefined && this.className.trim() !== "") {
      if (this.className.match(/^[0-9A-Za-z, ]*$/) != null) {
        this.classNameList = this.className.split(",");
        for (let i = 0; i < this.classNameList.length; i++) {
          this.classNameList[i] = this.classNameList[i].trim();
        }
      }
      else {
        this.msg.push("Błędnie wpisano nazwy klas");
        this.flagIfCorrect = false;
      }
    }
    else {
      this.msg.push("Nie wpisano nazwy klasy / klas");
      this.flagIfCorrect = false;
    }



    // Day of the week
    if (this.day == undefined) {
      this.msg.push("Nie wybrano dnia");
      this.flagIfCorrect = false;
    }

    // Begin time  
    if (this.beginTime != undefined) {
      this.beginTimeM = this.beginTime.split(':');
      if (!isNaN(+this.beginTimeM[0]) && !isNaN(+this.beginTimeM[1])) {
        if (+this.beginTimeM[0] < 8) {
          this.msg.push("Zajęcia nie mogą rozpoczynać się przed godziną 8:00");
          this.flagIfCorrect = false;
        }
      }
      else {
        this.msg.push("Wpisano błędną godzinę rozpoczęcia zajęć");
        this.flagIfCorrect = false;
      }
    }
    else {
      this.msg.push("Nie wpisano godziny rozpoczęcia zajęć");
      this.flagIfCorrect = false;
    }

    // Duration 

    if (this.durationM != undefined) {
      this.durationTemp = this.durationM.split(':');
      this.durationMinutes = (+this.durationTemp[0]) * 60 + (+this.durationTemp[1]);
      if (!isNaN(this.durationMinutes)) {
        if (this.durationMinutes > 600) {
          this.msg.push("Zajęcia nie mogą trwać dłużej niż 10 godzin");
          this.flagIfCorrect = false;
        }
      }
      else {
        this.msg.push("Wpisano błędny czas trwania zajęć");
        this.flagIfCorrect = false;
      }

    }
    else {
      this.msg.push("Nie wpisano czasu trwania zajęć");
      this.flagIfCorrect = false;

    }

    // Teachers 
    if (this.teachersM != undefined && this.teachersM.trim() !== "") {

      if (this.teachersM.match(/^[A-Za-z, ]*$/) != null) {
        this.teachersList = this.teachersM.split(',');
        for (let i = 0; i < this.teachersList.length; i++) {
          this.teachersList[i] = this.teachersList[i].trim();
        }
      }
      else {
        this.msg.push("Błędnie wpisano prowadzących");
        this.flagIfCorrect = false;
      }
    }
    else {
      this.msg.push("Nie wpisano prowadzących");
      this.flagIfCorrect = false;

    }

    if (!this.flagIfCorrect) {
      dialogConfig.data = { msg: this.msg };
      this.dialog.open(DialogInformationBoxComponent, dialogConfig);

    }
    else {
      let teachersString: String = "";
      this.teachersList.forEach(t => {
        if (teachersString == "")
          teachersString = t;
        else
          teachersString = teachersString + ", " + t;
      })
      let classTypeDB: String;
      switch (this.typeOfClassesN) {
        case ClassType.blackboardClass: classTypeDB = "blackboardClass"; break;
        case ClassType.laboratory: classTypeDB = "laboratory"; break;
        case ClassType.lecture: classTypeDB = "lecture"; break;
        case ClassType.other: classTypeDB = "other"; break;
        case ClassType.project: classTypeDB = "project"; break;
      }

      let classId = this.typeOfBox == 0 ? -1 : this.classId;
      this.newClassRaw = {
        id: classId,
        name: this.classesName,
        abbreviation: this.abbreviation,
        day: this.day,
        time: parseInt(this.beginTimeM[0].toString()) + ":" + parseInt(this.beginTimeM[1].toString()) + ":0",
        duration: this.durationMinutes,
        teachers: teachersString,
        node_id: this.workNodeId,
        classtype: classTypeDB
      }

      this.newClass.name = this.classesName;
      this.newClass.abbreviation = this.abbreviation;
      this.newClass.day = this.day;
      this.newClass.hour = parseInt(this.beginTimeM[0].toString());
      this.newClass.minute = parseInt(this.beginTimeM[1].toString());;
      this.newClass.duration = this.durationMinutes;
      this.newClass.classtype = this.typeOfClassesN;
      this.newClass.teachers = this.teachersList;
      this.newClass.nodeId = this.workNodeId;
      this.newClass.classrooms = this.classNameList;
      this.newClass.buildingName = this.buildingNameM;

      this.doUpdates(dialogConfig);
      
    }
  }

  private doUpdates(dialogConfig: MatDialogConfig<any>){
    // ADD/EDIT
    this.locationService.getClassroomsForLocations(this.curentLocation.id).subscribe(cl => {
      this.classrooms = cl as any[];

      for (let i = 0; i < this.classNameList.length; i++) {
        let found = false;
        this.classrooms.forEach(c => {
          if (c.name === this.classNameList[i]) {
            this.chosenClassrooms.push(c);
            found = true;
          }
        })

        if (!found) {
          this.msg.push("Sala o nazwie " + this.classNameList[i] + " nie istnieje na tym wydziale.");
          dialogConfig.data = { msg: this.msg };
          this.dialog.open(DialogInformationBoxComponent, dialogConfig);
          this.flagIfCorrect = false;
        }
      }

      if (this.flagIfCorrect) {
        if(this.typeOfBox == 0){
          this.scheduleService.postSchedule(this.newClassRaw).subscribe(result => {
            let clasroomAssociations: any[] = [];

            for (let cRoom of this.chosenClassrooms) {
              clasroomAssociations.push(this.scheduleService.associateClassroomSchedule(result['id'], cRoom.id));
            }

            forkJoin(clasroomAssociations).subscribe(
              res => {
                this.handleStatusZero(dialogConfig);
              },
              resAlt => {
                this.handleStatusZero(dialogConfig)
              })
          })
        } else {
          this.newClass.id = this.classId;

          this.scheduleService.updateSchedule(this.newClassRaw.id, this.newClassRaw).subscribe(result => {
            let oldClassrooms = [];
            let classroomsToAdd = [];
            let classroomsToDelete = [];

            this.scheduleService.getScheduleClassrooms(this.newClassRaw).subscribe( resClasrooms => {

              oldClassrooms = resClasrooms as any[];

              this.chosenClassrooms.forEach(cCls => {
                let classRoomMatch = oldClassrooms.find(oCls => oCls.name == cCls.name);
                if(classRoomMatch == undefined){
                  classroomsToAdd.push(cCls);
                }
              });

              oldClassrooms.forEach(oCls =>{

                let classRoomMatch = this.chosenClassrooms.find(cCls => oCls.name == cCls.name);
                if(classRoomMatch == undefined){
                  classroomsToDelete.push(oCls);
                }
              });

              let clasroomChanges: any[] = [];

              for (let cRoom of classroomsToAdd) {
                clasroomChanges.push(this.scheduleService.associateClassroomSchedule(result['id'], cRoom.id));
              }
              for (let cRoom of classroomsToDelete) {
                clasroomChanges.push(this.scheduleService.dissociateClassroomSchedule(result['id'], cRoom.id));
              }

              forkJoin(clasroomChanges).subscribe(
                res => {
                  this.handleUpdate(dialogConfig);
                },
                resAlt => {
                  this.handleUpdate(dialogConfig);
                });

                if(clasroomChanges.length == 0)
                  this.handleUpdate(dialogConfig);
            })
          })
        }
      }
    })
  }

  private handleUpdate(dialogConfig: MatDialogConfig<any>){
    // EDIT ACTION
    this.msg.push("Zedytowano istniejące zajęcia");
    dialogConfig.data = { msg: this.msg };
    this.dialog.open(DialogInformationBoxComponent, dialogConfig);
    this.dialogRef.close();
    window.location.reload();
  }

  private handleStatusZero(dialogConfig: MatDialogConfig<any>) {
    this.newClass.id = -1;
    this.msg.push("Dodano nowe zajęcia");

    // ADD ACTION

    window.location.reload();
    this.dialogRef.close();
  }

  delete() {

    this.msg = new Array();
    const dialogConfig = new MatDialogConfig();
    let msgres: any;

    if (this.msg.length == 0) {
      this.msg.push("Usunąć wybrane zajęcia?");
    }
    dialogConfig.data = { msg: this.msg, conf: 0 };
    const dialogRef_dial = this.dialog.open(DialogInformationBoxComponent, dialogConfig);

    dialogRef_dial.afterClosed().subscribe(result => {
      if (result != undefined) {
        msgres = result.data;
        if (msgres != null) {

          this.scheduleService.deleteSchedule(this.classId).subscribe(res => {
            this.dialogRef.close();
            window.location.reload();
          })
        }
      }
    }
    );
  }

  deleteConfirmed() {
    // DELETE ACTION 

    const dialogConfig = new MatDialogConfig();
    this.msg.pop();
    this.msg.push("Usunięto zajęcia");
    dialogConfig.data = { msg: this.msg };
    this.dialog.open(DialogInformationBoxComponent, dialogConfig);
  }
}
