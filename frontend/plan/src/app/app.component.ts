import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { titles } from './app-routing.module';
import { SharedVariables } from './services/shared-variables.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'plan';
  cardTitle = null;

  constructor(private router: Router, private globals: SharedVariables) { }

  ngOnInit() {
    this.router.events.subscribe(() => {
      let href = this.router.url;
      href = href.substr(1);
      
      let base = href.substr(0, href.indexOf('/'));
      if(base === "")
        base = href;

      for(let t of titles){
        if(t.path ===  base){
          this.cardTitle = t.title;
          break;
        }
      }
    });
  }
  changeGlobals() {
    this.globals.currentNode = null;
  }
}
