import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeViewComponent } from './components/welcome-view/welcome-view.component';
import { LoginViewComponent } from './components/login-view/login-view.component';
import { ScheduleTableComponent } from './components/schedule-table/schedule-table.component'
/*import { AuthGuard } from './utils/auth-guard';*/

// define base address here
const root = '';
const login = 'login';
const view = 'view';
const edit = 'edit';

// define routes using the base addresses above
const routes: Routes = [
  { path: root, component: WelcomeViewComponent },
  { path: login, component: LoginViewComponent},
  { path: view + '/:nodeId', component: ScheduleTableComponent },
  /*{ path: edit + '/:nodeId', component: Example1Component },   // examplary component for now*/
];

// Main Material card titles. Null = no title (the title box won't appear)
export const titles = [
  { path: root, title: null},
  { path: login, title: 'Logowanie'},
  { path: view, title: null},
  { path: edit, title: 'Edycja planu'},

  { path: '**', redirectTo: '' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
