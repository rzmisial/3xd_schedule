// Use this file to import all Material modules

import { NgModule } from '@angular/core';

// Material imports
// IMPORTANT!!! Make sure you import MODULES
//      MatSliderModule -> works
//      MatSlider -> blank website
import {
  MatSliderModule,
  MatFormFieldModule,
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatListModule,
  MatInputModule,
  MatTreeModule,
  MatIconModule,
  MatDialogModule,
  MatSelectModule
} from '@angular/material';
import { RippleGlobalOptions, MAT_RIPPLE_GLOBAL_OPTIONS } from '@angular/material';

// Ripple efect config (eg. for buttons). No touchie without a good reason.
const globalRippleConfig: RippleGlobalOptions = {
  disabled: true,
  /*
  animation: {
      enterDuration: 300,
      exitDuration: 0
  }
  */
};

// Add modules to import and export.
const importsExports = [
  MatSliderModule,
  MatFormFieldModule,
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatListModule,
  MatInputModule,
  MatTreeModule,
  MatIconModule,
  MatDialogModule,
  MatSelectModule
 ]
@NgModule({
  providers: [
    { provide: MAT_RIPPLE_GLOBAL_OPTIONS, useValue: globalRippleConfig }
  ],
  imports: importsExports,
  exports: importsExports
})
export class MaterialModule { }
