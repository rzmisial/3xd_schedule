export enum ClassType{
    lecture = "wyk.",
    laboratory = "lab.",
    blackboardClass = "ćw.",
    project = "proj.",
    other = "inne"
}

export enum ClassTypeDialog {
    lecture = "Wykład",
    laboratory = "Laboratorium",
    blackboardClass = "Ćwiczenia",
    project = "Projekt",
    other = "Inne"
  
  }