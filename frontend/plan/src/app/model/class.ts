import { ClassType } from './class.type';
import { Location } from './location';

export class Class{
    id: number;
    name: String;
    abbreviation: String;
    day: number;
    hour: number;
    minute: number;
    duration: number;
    teachers: String[];
    nodeId : number;
    // May need to move to another class(es)
    classtype: ClassType;
    classrooms: String[];
    buildingName: String;
    location: Location;
}