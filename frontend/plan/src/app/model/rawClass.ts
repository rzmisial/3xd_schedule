export class RawClass{
    id: number;
    name: String;
    abbreviation: String;
    day: number;
    time: String;
    duration: number;
    teachers: String;
    node_id : number;
    classtype: String;
}

/*
"id": self.id,
"name": self.name,
"abbreviation": self.abbreviation,
"classtype": self.classtype,
"teachers": self.teachers,
"day": self.day,
"time": str(self.time),
"duration": self.duration,
"node_id": self.node_id
*/