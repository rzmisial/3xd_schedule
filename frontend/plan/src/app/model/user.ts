export class User{
    id: number;
    password: string;
    email: string;
    access_token?: string;
}