import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from './material/material.module';
import { TaskbarComponent } from './components/navigation/taskbar/taskbar.component';
import { LoginViewComponent } from './components/login-view/login-view.component';
import { Example1Component } from './components/example1/example1.component';
import { WelcomeViewComponent } from './components/welcome-view/welcome-view.component';
import { NavigatorComponent } from './components/navigation/navigator/navigator.component';
import { AddDialogComponent } from './components/add-dialog/add-dialog.component';
import { DialogInformationBoxComponent } from './components/dialog-information-box/dialog-information-box.component';
import { ScheduleTableComponent } from './components/schedule-table/schedule-table.component';
import { MapComponent } from './components/map/map.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './utils/jwt-interceptor';
import { ErrorInterceptor } from './utils/error-interceptor';


@NgModule({
  declarations: [
    AppComponent,
    TaskbarComponent,
    LoginViewComponent,
    Example1Component,
    WelcomeViewComponent,
    NavigatorComponent,
    AddDialogComponent,
    DialogInformationBoxComponent,
    ScheduleTableComponent,
    MapComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    AddDialogComponent,
    DialogInformationBoxComponent,
    MapComponent
  ],
})
export class AppModule { }

